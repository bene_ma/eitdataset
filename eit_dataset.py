"""EITDataset dataset."""

from tensorflow.python.keras import backend as K
from tensorflow.python.framework import tensor_util
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import image_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.eager import context
import tensorflow_datasets as tfds
import tensorflow as tf
import tensorflow_addons as tfa

import scipy.io as sio
import numpy as np

from pathlib import Path

# Markdown description  that will appear on the catalog page.
_DESCRIPTION = """
**EIT Dataset** generated using EIDORS & Matlab with randomly generated features in a 3D-cylindrical tank
voltages are in the shape of (P, 16, 13, 1)
images are in the shape of (P, 64, 64, 1)

To support multiple FEM model's in the future, the simulation results of each model is stored separately. 
Currently, this dataset only contains one model: "cyl_h1200_d7500". A cylindrical model with height 120mm and densitiy 7.5mm

Each model contains a 80/20 test and validation split. The validation split is stored with the "val_" prefix. 
For example: "cyl_h1200_d7500" will retrieve the test split, while "val_cyl_h1200_d7500" retrieves the validation split.

Additionally, rotated variants using EIT domain rotations have been precomputed for the dataset and stored with the "_rot" postfix.
"cyl_h1200_d7500_rot" contains the same samples as "cyl_h1200_d7500" but with all 15 rotated versions for each sample.

The model data also contains a homogeneous measurement for each simulation, which is a measurement of just the background, without any objects inside.
"""


class eit_dataset(tfds.core.GeneratorBasedBuilder):
  """DatasetBuilder for EITDataset dataset."""

  dataset_path = Path('/media/datengrab1_4tb/eliasson/dataset/v2.5/')
  VERSION = tfds.core.Version('2.5.3')
  RELEASE_NOTES = {
    '1.0.0': 'Initial release.',
    '1.1.0': 'Renamed voltage to voltages and image to images. Changed voltage shape to (16, 13, 1) and image shape to (64, 64, 1) (from (208,) and (64, 64)',
    '1.2.0': 'Added real_world split',
    '1.3.0': 'Added train_rotated split. Removed real_world split (Data gets scrambled around)',
    '1.4.0': 'Added background noise data and ellip model data',
    '2.0.0': 'New dataset structure',
    '2.1.0': 'Better validation dataset split',
    '2.3.0': 'uniform object position',
    '2.4.0': 'after review',
    '2.4.5': 'better rotation with less artifacts at the boundary of the model',
    '2.4.6': 'fixed target image artifacts',
    '2.4.7': 'added perturbations',
    '2.5.0': 'added homogeneous measurement for every object configuration',
    '2.5.1': 'testing no homogeneous voltages',
    '2.5.2': 'added homogeneous measurements back in',
    '2.5.3': 'minor fixes, better documentation',
  }

  def _info(self) -> tfds.core.DatasetInfo:
    data = sio.loadmat(self.dataset_path / "parameters.mat")
    self.ci_pertubs = tf.reshape(tf.constant(data['ci_pertub_stds'], dtype=tf.float32), [-1])
    self.cond_pertubs = tf.reshape(tf.constant(data['cond_pertub_stds'], dtype=tf.float32), [-1])
    self.pos_pertubs = tf.reshape(tf.constant(data['pos_pertub_stds'], dtype=tf.float32), [-1])

    """Returns the dataset metadata."""
    return tfds.core.DatasetInfo(
      builder=self,
      description=_DESCRIPTION,
      features=tfds.features.FeaturesDict({
        'voltages': tfds.features.Tensor(shape=(16, 16, 13, 1), dtype=tf.float32), # 16 perturbed versions, 16x13x1 voltage data shape
        'image': tfds.features.Tensor(shape=(64, 64, 1), dtype=tf.float32),
      }),
      metadata=tfds.core.MetadataDict({ # some meta data added to the dataset.
        'image_nan_mask': (tf.image.resize(tf.constant(data['image_mask'], shape=(64, 64, 1), dtype=tf.uint8), [64, 64]) > 0).numpy().tolist(),
        'ci_pertub_stds': self.ci_pertubs.numpy().tolist(),
        'cond_pertub_stds': self.cond_pertubs.numpy().tolist(),
        'node_pos_pertub_stds': self.pos_pertubs.numpy().tolist(),
      }),
      supervised_keys=('voltages', 'image'),
    )

  def _split_generators(self, dl_manager: tfds.download.DownloadManager):
    """Returns SplitGenerators."""
    # Generate data
    # Also generate rotated data

    data = {}
    # Test split
    data.update({x.name:                self._generate_examples(x, val=False, rot=False) for x in self.dataset_path.iterdir() if x.is_dir()})
    data.update({x.name+"_rot":        self._generate_examples(x, val=False, rot=True)  for x in self.dataset_path.iterdir() if x.is_dir()})
    # Validation split
    data.update({"val_"+x.name:        self._generate_examples(x, val=True, rot=False) for x in self.dataset_path.iterdir() if x.is_dir()})
    data.update({"val_"+x.name+"_rot": self._generate_examples(x, val=True, rot=True)  for x in self.dataset_path.iterdir() if x.is_dir()})

    return data

  def _generate_examples(self, path, val=False, rot=False):
    """Yields measurement data. """

    print(f'Getting Examples for {path.name} with rotation={rot}')

    for key, data in self._get_data(path, validation=val, rotate=rot):

      # check for invalid data
      voltage_min, voltage_max = data['voltages'].min(), data['voltages'].max()
      image_min, image_max = data['image'].min(), data['image'].max()
      # voltage should not be negative
      if voltage_min < 0 or voltage_min == voltage_max:
        print(f'Err: Invalid voltage data! {key} min: {voltage_min}, max: {voltage_max}')
        continue
      # image should be in valid range
      if (image_min < 0 or image_min >= 5 or image_max < 0 or image_max >= 5):
        print(f'Err: Invalid iamge data! {key} min: {image_min}, max: {image_max}')
        continue

      # return the measurement with a unique key of '<file_name>_<index>'
      yield key, data

  def _get_data(self, model_path, validation=False, rotate=False):
    ''' This is the main function, which reads generated data from disk and converts it into the correct format '''

    # Each simulation is stored in its own file
    # Get all .mat files in the given folders
    for object_path in sorted(model_path.iterdir()):
      files = sorted(object_path.glob('*.mat'))
      num_files = len(files)
      
      # Get training/validation split cut index at 20%
      cut = int(num_files / 5)
      print(object_path.name, num_files, cut)
      if validation:
        files = files[:cut]
      else:
        files = files[cut:]

      # Go through all the files, yield each example
      for file_path in files:
        key = f'{model_path.name}/{object_path.name}/{file_path.name}'

        # Load the .mat file
        data = sio.loadmat(file_path)

        # conductivity image
        image = tf.constant(data['image'], shape=[64, 64, 1], dtype=tf.float32)
        # mask is a uint8 image, which tells us, at which pixel which objectID is. 
        # a pixel value of 0 means background, 1 is the first object and so on.
        # this is very useful for proper rotation, we are rotating the mask and assign the correct conductivity
        # to each pixel. This way we wont get blurry edges when rotating
        mask = tf.constant(data['objects_mask'], shape=[64, 64, 1], dtype=tf.uint8)
        # A list of objects and their information
        objects = data['objects']
        # voltage measurements
        voltages = data['voltages']

        # The image generation of EIDORS left a lot to desire.. 
        # Since we have a mask, we can generate our own, much nicer image with sharper edges at object boundaries
        image, mask = fix_image(image, mask, objects)
 
        # We have multiple voltage measurements in the dataset. 'no_pertubation', 'ci_pertubation', 'cond_pertubation', 'node_pos_pertubation'.
        # convert each into the correct shape of [N, 16, 13, 1], where N is the number of perturbed measurements
        voltages_no_pertub = tf.stack([tf.constant(x[...,0], shape=[16, 13, 1], dtype=tf.float32) for x in voltages['no_pertubation'][0, 0][..., 0]])
        voltages_ci_pertub = tf.stack([tf.constant(x[...,0], shape=[16, 13, 1], dtype=tf.float32) for x in voltages['ci_pertubation'][0, 0][...,0]])
        voltages_co_pertub = tf.stack([tf.constant(x[...,0], shape=[16, 13, 1], dtype=tf.float32) for x in voltages['cond_pertubation'][0, 0][...,0]])
        voltages_np_pertub = tf.stack([tf.constant(x[...,0], shape=[16, 13, 1], dtype=tf.float32) for x in voltages['node_pos_pertubation'][0, 0][...,0]])

        # Now we can concatenate those measurements, resulting ing [N, 16, 13, 1] combined measurements. notice, that 'voltages_no_pertub' is always at index 0
        voltages_stacked = tf.concat(values=[voltages_no_pertub, voltages_ci_pertub, voltages_co_pertub, voltages_np_pertub], axis=0)

        if rotate:
          # return rotated versions of the original with rotations from 1 to 15
          # the original will not be included here
          for rotation in range(1, 16):
            voltages_stacked_rot, image_rot = rotate_eit_data(voltages_stacked, image, mask, objects, rotation)
            # yield each example, one after another
            yield (f'{key}_{rotation}', 
              {
                'voltages': voltages_stacked_rot.numpy(),
                'image': image_rot.numpy(),
              })
        else:
          # yield each example, one after another
          yield (f'{key}_0',
          {
            'voltages': voltages_stacked.numpy(),
            'image': image.numpy(),
          })
          # Yield homogeneous measurement
          yield (f'{key}_h',
          {
            'voltages': tf.repeat(tf.constant(voltages['homogeneous'][0,0][0,0][...,0], shape=[1, 16, 13, 1], dtype=tf.float32), voltages_stacked.shape[0], axis=0).numpy(),
            'image': tf.where(tf.math.is_nan(image), float('nan'), tf.ones(shape=[64, 64, 1], dtype=tf.float32) * objects[0]['conductivity'][0][0,0]).numpy()
          })

def rotate_eit_data(voltages, image , mask, objects, shift):
  ''' Rotate the data using EIT domain rotations with the given shift'''
  # shift the voltage measurement in the batch by the shift amount on the first axis
  new_voltages = tf.roll(voltages, shift=shift, axis=1)
  # image rotatation by a 16th of a rotation per shift
  new_image = rotate_image(image, mask, objects, tf.constant(shift * -2 * np.pi / 16, dtype=tf.float32))

  return new_voltages, new_image

def rotate_image(image, mask, objects, angle):
    nan_mask = tf.math.is_nan(image)
    # Generate conductivites array and add background conductivity
    if len(objects) > 0:
        conductivities = [obj['conductivity'][0, 0] for obj in objects[0]]
    else:
        conductivities = []

    # rotate the mask
    mask = tf.where(nan_mask, 0, mask)
    rotated_mask = tfa.image.rotate(mask, angle)
    
    # create rotated image
    rotated_image = image
    for index, conductivity in enumerate(conductivities):
        rotated_image = tf.where(rotated_mask == index, conductivity, rotated_image)
    rotated_image = tf.where(nan_mask, float('nan'), rotated_image)

    return rotated_image

def fix_mask(mask):
    ''' uses morphological closing to remove single stray pixels in the mask'''
    new_mask = tf.zeros(mask.shape, mask.dtype)
    kernel = tf.ones([5, 5, 1], tf.int8) 
    
    # We have up to five objects (currently). Apply closing to each object independently
    for object_id in range(1, 5):
        # get object mask for object number 'index'
        object_mask = tf.cast(tf.expand_dims(mask == object_id, axis=0), tf.int8)
        # check for early abort. If there are no pixels, we have computed all objects
        if tf.reduce_sum(tf.cast(object_mask, tf.uint8)) == 0:
            break
        
        # Use morphological closing to reduce noisy edges
        object_mask = tf.nn.dilation2d(object_mask, kernel, strides=[1,1,1,1], padding='SAME', data_format='NHWC', dilations=[1,1,1,1])
        object_mask = tf.nn.erosion2d(object_mask, kernel, strides=[1,1,1,1], padding='SAME', data_format='NHWC', dilations=[1,1,1,1])
        
        # apply new mask. convert it to boolean mask
        object_mask = object_mask > 0
        # set new_mask to object_id where the object_mask equals True
        new_mask = tf.where(object_mask[0], object_id, new_mask)
        
    return new_mask

def fix_image(image, mask, objects):
    ''' Create a new image using the old image and a mask of object ID's'''
    # apply closing to mask in order to remove stray pixels
    new_mask = fix_mask(mask)
    new_image = tf.ones(image.shape, image.dtype)
    for object_id in range(len(objects)):
        # get object mask for object number 'object_id'
        obj_mask = (new_mask == object_id)

        # set all pixels where obj_mask == object_id to the same conductivity value
        new_image = tf.where(obj_mask, objects[object_id]['conductivity'][0][0,0], new_image)
    
    # add nans at the corners into the new image
    new_image = tf.where(tf.math.is_nan(image), float('nan'), new_image)
    return new_image, new_mask